package com.example.power.chapter8.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EmployeeService.class})
public class EmployeeServiceTest {

    /**
     * mock 是不会被真实执行的，但是 spy 会
     */
    @Test
    public void foo() {
        EmployeeService service = mock(EmployeeService.class);
        service.foo("tingfeng");
    }

    /**
     * spy 示例，匹配
     */
    @Test
    public void fooWithSpy() {
        EmployeeService service = spy(new EmployeeService());
        String arg = "tingfeng";
        doNothing().when(service).foo(arg);
//        service.foo(arg);       // 参数符合，hello() 方法不执行
        service.foo("听风");  // 参数不符合，hello()方法执行
    }

    /**
     * spy 示例
     */
    @Test
    public void checkExist() throws Exception {
        EmployeeService service = spy(new EmployeeService());
        doReturn(true).when(service, "checkExist", "听风");

//        assertTrue(service.exist("听风"));
        assertTrue(service.exist("LRK"));
    }
}