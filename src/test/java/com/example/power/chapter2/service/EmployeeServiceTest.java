package com.example.power.chapter2.service;

import com.example.power.chapter2.dao.EmployeeDao;
import com.example.power.common.model.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EmployeeService.class})
class EmployeeServiceTest {

    public EmployeeServiceTest() {
    }

    /**
     * 有返回值测试
     */
    @Test
    public void testGetCountEmployee() throws Exception {
        EmployeeService service = new EmployeeService();
        // mock 对象
        EmployeeDao dao = PowerMockito.mock(EmployeeDao.class);
        // 创建无参对象
        PowerMockito.whenNew(EmployeeDao.class).withNoArguments().thenReturn(dao);
        // 执行有返回值方法
        PowerMockito.doReturn(10).when(dao).getCount();
        // 执行方法
        int count = service.getTotalEmployee();
        Assert.assertEquals(10, count);
    }

    /**
     * 无返回值测试
     */
    @Test
    public void testSaveEmployee() throws Exception{
        EmployeeService service = new EmployeeService();
        // mock 对象
        EmployeeDao dao = PowerMockito.mock(EmployeeDao.class);
        // 创建无参对象
        PowerMockito.whenNew(EmployeeDao.class).withNoArguments().thenReturn(dao);
        // 执行无返回值方法
        Employee employee = new Employee();
        PowerMockito.doNothing().when(dao).insertEmployee(employee);
        // 执行
        service.createEmployee(employee);
        // 次数验证
        Mockito.verify(dao, Mockito.times(1)).insertEmployee(employee);
    }
}
