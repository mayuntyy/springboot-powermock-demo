package com.example.power.chapter3.service;

import com.example.power.chapter3.dao.EmployeeDao;
import com.example.power.common.model.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.mockito.PowerMockito.*;

/**
 * 静态方法测试
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({EmployeeDao.class})
public class EmployeeServiceTest {

    @Test
    public void getTotalEmployee() throws Exception{
        // mock 静态对象
        mockStatic(EmployeeDao.class);
        when(EmployeeDao.getCount()).thenReturn(10);
        EmployeeService service = new EmployeeService();
        int totalEmployee = service.getTotalEmployee();
        Assert.assertEquals(10, totalEmployee);
    }

    @Test
    public void createEmployee() throws Exception{
        // mock 静态对象
        mockStatic(EmployeeDao.class);
        Employee employee = new Employee();
        doNothing().when(EmployeeDao.class);

        EmployeeService service = new EmployeeService();
        service.createEmployee(employee);
    }
}