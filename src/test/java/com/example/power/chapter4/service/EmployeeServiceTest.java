package com.example.power.chapter4.service;

import com.example.power.chapter4.dao.EmployeeDao;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EmployeeService.class,EmployeeDao.class})
public class EmployeeServiceTest {

    /**
     * 使用 powerMock 方式
     */
    @Test
    public void getTotalEmployeeWithPowerMock() {
        // mock final修饰的 Dao层
        EmployeeDao dao = PowerMockito.mock(EmployeeDao.class);
        System.out.println(dao.getClass());
        PowerMockito.when(dao.getCount()).thenReturn(10);
        EmployeeService service = new EmployeeService(dao);
        int totalEmployee = service.getTotalEmployee();
        Assert.assertEquals(10, totalEmployee);
    }

    @Test
    public void createEmployee() {
    }
}