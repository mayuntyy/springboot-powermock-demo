package com.example.power.chapter4.service;

import com.example.power.chapter4.dao.EmployeeDao;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

/**
 * Mockito 方式测试
 */
public class EmployeeServiceWithMockitoTest {

    @Mock
    private EmployeeDao dao;

    /**
     * 使用 mockito 方式（final类型无效）
     */
    @Test
    public void getTotalEmployeeWithMockito() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(dao.getCount()).thenReturn(10);
        EmployeeService service = new EmployeeService(dao);
        int totalEmployee = service.getTotalEmployee();
        Assert.assertEquals(10, totalEmployee);
    }

}