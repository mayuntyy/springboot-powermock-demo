package com.example.power.chapter1.service;

import com.example.power.chapter1.dao.EmployeeDao;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;

public class EmployeeServiceWithMockitoTest {

    private EmployeeService service;

    @Mock
    private EmployeeDao employeeDao;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        service = new EmployeeService(employeeDao);
    }

    /**
     * mockito 测试（通过）
     */
    @Test
    public void testGetTotalEmployeeWithMock() {
        // 执行 getTotal 返回 10
        Mockito.when(employeeDao.getTotal()).thenReturn(10);
        int total = service.getTotalEmployee();
        assertEquals(10, total);
    }

}
