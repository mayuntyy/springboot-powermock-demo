package com.example.power.chapter1.service;

import com.example.power.chapter1.dao.EmployeeDao;
import com.example.power.common.model.Employee;
import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;

import static org.junit.Assert.assertEquals;

public class EmployeeServiceTest {

    /**
     * 无 mock 测试 （不通过）
     */
    @Test
    public void testGetTotalEmployee() {
        final EmployeeService service = new EmployeeService(new EmployeeDao());
        int total = service.getTotalEmployee();
        assertEquals(10, total);
    }

    /**
     * powerMock 有返回值（通过）
     * <p>
     * 语法：
     * 1、do...when...then
     * 2、when...()...then
     */
    @Test
    public void testGetTotalEmployeeWithMock() {
        // mock 一个 EmployeeDao 对象
        EmployeeDao employeeDao = PowerMockito.mock(EmployeeDao.class);
        // 执行 getTotal 返回 10
        PowerMockito.doReturn(10).when(employeeDao).getTotal();
        // 等同于
        //PowerMockito.when(employeeDao.getTotal()).thenReturn(10);

        EmployeeService service = new EmployeeService(employeeDao);
        int total = service.getTotalEmployee();
        assertEquals(10, total);
    }

    /**
     * powerMock 无返回值（通过）
     *
     */
    @Test
    public void testCreateEmployee() {
        EmployeeDao employeeDao = PowerMockito.mock(EmployeeDao.class);
        Employee employee = new Employee();
        PowerMockito.doNothing().when(employeeDao).addEmployee(employee);
        EmployeeService service = new EmployeeService(employeeDao);
        service.createEmployee(employee);

        // 校验被 mock 出来的对 象中的某个方法是否被调用
        Mockito.verify(employeeDao).addEmployee(employee);
    }

}
