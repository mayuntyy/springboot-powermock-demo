package com.example.power.chapter5.service;

import com.example.power.chapter5.dao.EmployeeDao;
import com.example.power.common.model.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EmployeeService.class})
public class EmployeeServiceTest {

    @Test
    public void saveOrUpdate() throws Exception {
        // mock对象
        Employee employee = mock(Employee.class);
        EmployeeDao dao = mock(EmployeeDao.class);
        whenNew(EmployeeDao.class).withAnyArguments().thenReturn(dao);
//        when(dao.getCount(employee)).thenReturn(10);
        when(dao.getCount(employee)).thenReturn(0);
        EmployeeService service = new EmployeeService();
        service.saveOrUpdate(employee);

        // 断言 执行
        Mockito.verify(dao).insertEmployee(employee);
        // never 绝对不会执行
        Mockito.verify(dao, Mockito.never()).updateEmployee(employee);
    }
}