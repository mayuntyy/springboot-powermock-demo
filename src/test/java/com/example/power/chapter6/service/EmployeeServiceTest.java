package com.example.power.chapter6.service;

import com.example.power.chapter6.dao.EmployeeDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.powermock.api.mockito.PowerMockito.*;

/**
 * Dao 层构造函数
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({EmployeeService.class})
public class EmployeeServiceTest {

    @Test
    public void login() throws Exception{
        EmployeeDao dao = mock(EmployeeDao.class);
        String username="tingfeng";
        String password="123";
        whenNew(EmployeeDao.class).withArguments(username, password).thenReturn(dao);
        doNothing().when(dao).insert();

        EmployeeService service = new EmployeeService();
        service.login(username, password);

        Mockito.verify(dao).insert();
    }
}