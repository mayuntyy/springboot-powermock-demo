package com.example.power.chapter7.service;

import com.example.power.chapter7.dao.EmployeeDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.powermock.api.mockito.PowerMockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EmployeeService.class})
public class EmployeeServiceTest {

    @Test
    public void find() throws Exception {
        EmployeeDao dao = mock(EmployeeDao.class);
        whenNew(EmployeeDao.class).withAnyArguments().thenReturn(dao);

        EmployeeService service = new EmployeeService();

        when(dao.queryByName("tingfeng")).thenReturn("听风");
        String result = service.find("tingfeng");
        assertEquals("听风", result);

        when(dao.queryByName("chengxumiao")).thenReturn("程序喵");
        result = service.find("chengxumiao");
        assertEquals("程序喵", result);

        when(dao.queryByName("LRK")).thenReturn("刘仁奎");
        result = service.find("LRK");
        assertEquals("刘仁奎", result);
    }

    /**
     * ArgumentMatcher 参数匹配
     */
    static class MyArgumentMatcher implements ArgumentMatcher<String> {
        @Override
        public boolean matches(String argument) {
            switch (argument) {
                case "tingfeng":
                case "chengxumiao":
                case "LRK":
                    return true;
                default:
                    return false;
            }
        }
    }

    @Test
    public void findWithMatcher() throws Exception {
        EmployeeDao dao = mock(EmployeeDao.class);
        whenNew(EmployeeDao.class).withAnyArguments().thenReturn(dao);

        String result = "听风";
        when(dao.queryByName(argThat(new MyArgumentMatcher()))).thenReturn(result);

        EmployeeService service = new EmployeeService();
        assertEquals(result, service.find("tingfeng"));
        assertEquals(result, service.find("chengxumiao"));
        assertEquals(result, service.find("LRK"));
//        assertEquals(result, service.find("Demo"));
    }

    /**
     * Answer 匹配结果
     */
    @Test
    public void findWithAnswer() throws Exception {
        EmployeeDao dao = mock(EmployeeDao.class);
        whenNew(EmployeeDao.class).withAnyArguments().thenReturn(dao);
        when(dao.queryByName(anyString())).then(invocation -> {
            String arg = (String) invocation.getArguments()[0];
            switch (arg) {
                case "tingfeng":
                    return "你好，听风";
                case "chengxumiao":
                    return "你好，程序喵";
                case "LRK":
                    return "你好，刘仁奎";
                default:
                    throw new RuntimeException("Not support " + arg);
            }
        });

        EmployeeService service = new EmployeeService();
        assertEquals("你好，听风", service.find("tingfeng"));
        assertEquals("你好，程序喵", service.find("chengxumiao"));
        assertEquals("你好，刘仁奎", service.find("LRK"));
//        assertEquals("你好", service.find("hello"));
    }

}