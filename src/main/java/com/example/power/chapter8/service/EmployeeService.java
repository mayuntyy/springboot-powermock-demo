package com.example.power.chapter8.service;

public class EmployeeService {

    public void foo(String arg) {
        hello(arg);
    }

    /**
     * 私有方法
     */
    private void hello(String name) {
        System.out.println("Hello, I am ." + name);
    }

    public boolean exist(String username) {
        return checkExist(username);
    }

    /**
     * 私有方法
     */
    private boolean checkExist(String username) {
        throw new UnsupportedOperationException();
    }
}
