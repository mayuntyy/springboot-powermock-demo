package com.example.power.chapter5.dao;

import com.example.power.common.model.Employee;

public class EmployeeDao {

    /**
     * 有返回值
     */
    public int getCount(Employee employee) {
        throw new UnsupportedOperationException();
    }

    /**
     * 无返回值
     */
    public void updateEmployee(Employee employee) {
        throw new UnsupportedOperationException();
    }

    /**
     * 无返回值
     */
    public void insertEmployee(Employee employee) {
        throw new UnsupportedOperationException();
    }
}
