package com.example.power.chapter5.service;

import com.example.power.chapter5.dao.EmployeeDao;
import com.example.power.common.model.Employee;

public class EmployeeService {

    /**
     * 无返回值
     */
    public void saveOrUpdate(Employee employee) {
        EmployeeDao dao = new EmployeeDao();
        if (dao.getCount(employee) > 0) {
            dao.updateEmployee(employee);
        } else {
            dao.insertEmployee(employee);
        }
    }
}
