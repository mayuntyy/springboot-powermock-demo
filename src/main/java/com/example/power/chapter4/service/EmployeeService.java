package com.example.power.chapter4.service;

import com.example.power.chapter4.dao.EmployeeDao;
import com.example.power.common.model.Employee;

public class EmployeeService {

    private EmployeeDao employeeDao;

    public EmployeeService(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    public int getTotalEmployee() {
        return employeeDao.getCount();
    }

    public void createEmployee(Employee employee) {
        employeeDao.insertEmployee (employee);
    }
}
