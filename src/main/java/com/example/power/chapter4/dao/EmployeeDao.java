package com.example.power.chapter4.dao;

import com.example.power.common.model.Employee;

/**
 * final 类型 Dao
 */
final public class EmployeeDao {

    /**
     * 有返回值
     */
    public int getCount() {
        throw new UnsupportedOperationException();
    }

    /**
     * 无返回值
     */
    public void insertEmployee(Employee employee) {
        throw new UnsupportedOperationException(); }
}
