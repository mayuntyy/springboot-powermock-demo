package com.example.power.chapter3.service;

import com.example.power.chapter3.dao.EmployeeDao;
import com.example.power.common.model.Employee;

/**
 * 静态方法执行 dao 方法
 */
public class EmployeeService {

    public int getTotalEmployee() {
        return EmployeeDao.getCount();
    }

    public void createEmployee(Employee employee) {
        EmployeeDao.insertEmployee (employee);
    }
}
