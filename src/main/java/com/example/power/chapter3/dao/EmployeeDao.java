package com.example.power.chapter3.dao;

import com.example.power.common.model.Employee;

public class EmployeeDao {

    /**
     * 有返回值
     */
    public static int getCount() {
        throw new UnsupportedOperationException();
    }

    /**
     * 无返回值
     */
    public static void insertEmployee(Employee employee) {
        throw new UnsupportedOperationException(); }
}
