package com.example.power.chapter2.dao;

import com.example.power.common.model.Employee;

public class EmployeeDao {

    /**
     * 有返回值
     */
    public int getCount() {
        throw new UnsupportedOperationException();
    }

    /**
     * 无返回值
     * @param employee
     */
    public void insertEmployee(Employee employee) {
        throw new UnsupportedOperationException(); }
}
