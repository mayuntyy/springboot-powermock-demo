package com.example.power.chapter2.service;

import com.example.power.chapter2.dao.EmployeeDao;
import com.example.power.common.model.Employee;

/**
 * 使用 new EmployeeDao 方式执行 dao 方法
 */
public class EmployeeService {

    /**
     * 获取所有员工的数量
     */
    public int getTotalEmployee() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getCount();
    }

    /**
     * 无返回值
     *
     * @param employee
     */
    public void createEmployee(Employee employee) {
        EmployeeDao employeeDao = new EmployeeDao();
        employeeDao.insertEmployee(employee);
    }
}
