package com.example.power.chapter1.dao;

import com.example.power.common.model.Employee;

public class EmployeeDao {

    /**
     * 有返回值
     */
    public int getTotal() {
        throw new UnsupportedOperationException();
    }

    /**
     * 无返回值
     * @param employee
     */
    public void addEmployee(Employee employee) {
        throw new UnsupportedOperationException(); }
}
