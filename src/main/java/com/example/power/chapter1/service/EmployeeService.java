package com.example.power.chapter1.service;

import com.example.power.chapter1.dao.EmployeeDao;
import com.example.power.common.model.Employee;

/**
 * 构造注入方式
 */
public class EmployeeService {

    private EmployeeDao employeeDao;

    public EmployeeService(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    /**
     * 获取所有员工的数量
     */
    public int getTotalEmployee() {
        return employeeDao.getTotal();
    }

    /**
     * 无返回值
     * @param employee
     */
    public void createEmployee(Employee employee) {
        employeeDao.addEmployee(employee); }
}
