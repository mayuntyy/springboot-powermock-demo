package com.example.power.chapter7.service;

import com.example.power.chapter7.dao.EmployeeDao;

public class EmployeeService {

    public String find(String name){
        EmployeeDao dao = new EmployeeDao();
        return dao.queryByName(name);
    }
}
